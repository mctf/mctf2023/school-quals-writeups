from pyzbar.pyzbar import decode, ZBarSymbol
from requests import Session
from PIL import Image, ImageFile
import io
import re
import base64
import numpy as np

HOST = "qr-code-carousel.mctf.ru"

session = Session()

def iterate(HOST):
    r = session.get('http://'+HOST)
    base64_string = re.search(r'data:image/png;base64,(.*?)"', r.text).group(1)
    qrcode = Image.open(io.BytesIO(base64.b64decode(base64_string)))
    decoded_qr = decode(qrcode)[0].data
    text = decoded_qr.decode()
    if "MCTF{" not in text:
        iterate(text)
    else:
        print(text)

iterate(HOST)