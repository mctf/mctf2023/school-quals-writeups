import re
import json
import requests
from pwn import remote
from tqdm import tqdm
from Levenshtein import distance as lev


def DownloadPokemonsInfo():
    allPokemons = []

    url = "https://pokeapi.co/api/v2/pokemon?limit=100000&offset=0"
    pokemons_info = "https://pokemon.gameinfo.io/en/pokemon/"

    pokemon_names = [i["name"] for i in requests.get(url).json()["results"]]

    for pokemon in tqdm(pokemon_names):
        pokemon_info = requests.get(f"{pokemons_info}{pokemon}").text
        pokemon_bio = re.findall(r'<p class="description">\n.*', pokemon_info)

        try:
            allPokemons.append(
                {
                    "Name": pokemon.capitalize(),
                    "Bio": pokemon_bio[1][31:-1:].replace(
                        pokemon.capitalize(), "*" * len(pokemon)
                    ),
                }
            )
        except IndexError:
            try:
                allPokemons.append(
                    {
                        "Name": pokemon.capitalize(),
                        "Bio": pokemon_bio[0][31:-1:].replace(
                            pokemon.capitalize(), "*" * len(pokemon)
                        ),
                    }
                )
            except IndexError:
                pass

    with open("pokemons.json", "w") as outfile:
        json.dump(allPokemons, outfile, indent=4)


def Solution():
    file = open("pokemons.json")
    PokemonsData = json.load(file)
    file.close()

    conn = remote("localhost", 10000)

    while True:
        recv = conn.recv().decode("utf-8")

        if "mctf" in recv.lower():
            print(recv)
            break

        answer = PokemonsData[0]["Name"]
        score = lev(recv, PokemonsData[0]["Bio"])

        for Pokemon in PokemonsData:
            lev_dist = lev(recv, Pokemon["Bio"])

            if score > lev_dist:
                answer = Pokemon["Name"]
                score = lev_dist

        conn.send(f"{answer}\n".encode("utf-8"))
        conn.recvline().decode("utf-8")


if __name__ == "__main__":
    Solution()
