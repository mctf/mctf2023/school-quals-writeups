## Тряхнем стариной

Собрались мы с другом пойти в кафешку, все бы ничего, но он притащил с собой опять этого интернет-гика  
Оооо, этот интернет гик, все мозги извел, нашел какой-то старый вирусный мем, угорал над ним весь вечер  
Я решил тоже посмотреть что же это такое, сказал, что мне нужно в туалет, и взял с собой телефон, но я помню только эти слова: “начало, Compuserve, ты точно поймешь, что это оно”  
Поможешь мне найти его?  

флаг: mctf{второе слова из самого длинного названия этого мема из википедии}


## Hint

Я вспомнил, что этот гик еще говорил слово “ребенок”, может это тебе поможет

## Writeups

Берем слова гика из описания и идем гуглить, благодаря слову "начало" предполагаем, что это какой-то старый известный мем с сайта Compuserve

Можно гуглить по обычному, можно при помощи гугл дорксов, вот пару примеров запросов:
```
"compuserve" which memes blew up the internet
"compuserve" old popular memes
```

Немного полазив по сайтам, на некоторых можно увидеть список старых популярных мемов

![1](images/1.png)

![2](images/2.png)

Гуглим каждого из них, не забывая описание таска

После поиска у нас остается только один подходящий мем: Dancing Baby

Тк нам нужно название из википедии, то ищем статью на википедии с данным мемом и берем второе слово из самого длинного названия, это слово Oogachacka, оборачиваем в mctf{} и таск решен!

![3](images/3.jpg)


Так же при некоторых запросах можно попасть сразу на  Dancing Baby

![4](images/4.jpg)

## Flag

mctf{Oogachacka}

## Complexity

Medium