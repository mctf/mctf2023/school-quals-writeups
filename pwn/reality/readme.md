# PWN | hard |  reality
RU:
Ну тут всё просто - либо можешь либо не можешь. Давай, сделай это...

EN:
Well, it's simple, you either can or you can't. Go ahead, do it..

[nc task.pwn.example.com 28888]

# Flag
MCTF{R3al1ty_can-b3-what3v3r-I-want}


# Administration

`28888` - порт для подключения по нкату

`reality.zip` - файлы, которые нужно отдать игрокам

Сам бинарь: **./chal/reality**


## Launch

    # docker-compose build
    # docker-compose up -d

## Check working

    $ python3 ./chal/solve.py REMOTE # Для проверки решения на хосте
    $ python3 ./chal/solve.py LOCAL 

# Writeup

<details>

  <summary>Click me</summary>
  Решение в ./solve.py

</details>

