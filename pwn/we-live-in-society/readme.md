# PWN | Medium |  We live in society
RU:
Мы живём в обществе где арчеводов призерают

EN:
We live in a society where arch users are reviled

[nc task.pwn.example.com 18888]

# Flag
MCTF{Pretty-easy-was-it-padawan}


# Administration

`18888` - порт для подключения по нкату

`we_live_in_society.zip` - файлы, которые нужно отдать игрокам

Сам бинарь: **./chal/we_live_in_society**


## Launch

    # docker-compose build
    # docker-compose up -d

## Check working

    $ python3 ./chal/solve.py REMOTE # Для проверки решения на хосте
    $ python3 ./chal/solve.py LOCAL 

# Writeup

<details>

  <summary>Click me</summary>
  Решение в ./chal/solve.py

</details>

