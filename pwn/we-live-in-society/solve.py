from pwn import args, ELF, process, remote, gdb, context

PATH = "./we_live_in_society"
IP = 'localhost'
PORT = 18888
elf = context.binary = ELF(PATH, checksec=False)
GDBSCRIPT = '''b *main
c
'''


def conn():
    if args.GDB:
        return gdb.debug(PATH, gdbscript=GDBSCRIPT)
    elif args.REMOTE:
        return remote(IP, PORT)
    elif args.LOCAL:
        pty = process.PTY
        return process(PATH, stdin=pty, stdout=pty)


p = conn()

p.readuntil(b">")

p.sendline(b"ubuntu")
p.clean()

p.interactive()
