
## Don't trust your eyes
Нам в руки попала флешка негодяя, укравшего файлы с наших машин. Мы едва успели, он уже начал уничтожать улики. Помоги нам выяснить, было ли украдено что-то конфиденциальное

## Writeup
Нам дан образ флешки. Расширение .dd подсказывает, что с флешки была снята именно побайтовая копия. [Статейка](https://habr.com/ru/articles/117050/) на Хабре про побайтовое копирование.

Примонтируем образ и посмотрим, что у нас есть. Стандартными средствами винды это сделать не получится, используем [OSFMount](https://www.osforensics.com/tools/mount-disk-images.html). Лучше поставить галочку на режим read-only, чтобы не внести изменений в улику.  
![](screenshots/2.PNG)  
Знаем, что злоумышленник уже начал подчищать следы. Так как у нас побайтовая копия, то мы можем найти следы удаления файла (если он не был затерт) . Для этого удобно использовать модуль для Powershell - [PowerForensics](https://powerforensics.readthedocs.io/en). 

Поищем файл, для которого значение атрибута Deleted стоит True
```
Get-ForensicFileRecord -VolumeName E: | Where-Object {$_.Deleted}
```
![](screenshots/4.PNG)
Восстановим файл, достав содержимое атрибута Data через файловую запись.
```
$file_record = Get-ForensicFileRecord -VolumeName E: -Index 44 
$file_descriptor=$file_record.Attribute | Where-Object {$_.name -eq 'DATA'}
$start_cluster = $file_descriptor.DataRun | select *
Invoke-ForensicDD -InFile \\.\E: -Offset ($start_cluster.StartCluster*4096) -BlockSize ($start_cluster.ClusterLength*4096) -Count 1 -OutFile D:\secret_cat_restored.jpg
```
Получаем удаленную картинку, но она не открывается, возможно файл был намеренно поврежден. Откроем в hex-редакторе. Обратим внимание, что сигнатура .jpg файла (первые 12 байт) побита. Обратимся к [таблице](https://en.m.wikipedia.org/wiki/List_of_file_signatures) файловых сигнатур и восстановим ее.  
![](screenshots/5.PNG)  


Теперь картинка открывается и мы можем увидеть флаг:)


### Альтернативный (более простой) способ
Есть практически универсальная утилита для анализа образов и дампов - AccessData FTK Imager. С ее помощью можно пропустить этап монтирования и поиска удаленного файла вручную. Добавим образ как источник улик, развернем дерево содержимого и в корневой папке увидим удаленный файл. Экспортируем его и восстанавливаем способом, описанным выше.


## Flag
```
MCTF{s0m3t1m3s_d3l3t1ng_1s_n0t_3n0ugh}
```

## Complexity
Medium
