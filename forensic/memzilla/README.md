## memzilla
Таск на анализ дампа оперативной памяти.

## Ссылка на Таск
https://disk.yandex.ru/d/uTCSvgy0Z2czSA

## Writeups
Нам дан дамп оперативной памяти. Райтап будет написан для volatility2.

Начинаем анализ с плагина imageinfo, получаем профиль ОС для дальнейшей работы.
```
python2 vol.py -f memzilla.mem imageinfo
```

Смотрим процессы
```
python2 vol.py -f memzilla.mem --profile=Win7SP1x64 pstree
```
Получив великое разнообразие процессов, начинаем поиск. Нас интересуют только запущенные от explorer.exe

![pstree](./static/pstree.png)


Флаг разделен на 3 части: 

1. История chrome
2. Изображение в paint
3. Надпись в notepad++

Распишу поиск каждой части по отдельности.

**1. История chrome**
Часть флага лежит в истории хрома. Можно воспользоваться strings, но будет проще [найти плагин](https://github.com/superponible/volatility-plugins/blob/master/chromehistory.py).
```
python2 vol.py -f memzilla.mem --profile=Win7SP1x64 chromehistory
```
![chromehistory](./static/chromehistory1.png)

Видим перевернутую base64 строку, переворачиваем, декодим и получаем первую часть флага. Так-же видим подсказку для решения второй части флага.

**2. Изображение в paint**
Для получения изображения нам необходимо воспользоваться плагином memdump на процесс mspaint.exe, поменять разрешение на .data и залить в gimp с заранее известным нам разрешением. (см. подсказку)
```
python2 vol.py -f memzilla3.mem --profile=Win7SP1x64 memdump --pid=3728 --dump-dir=./dumps
```

Играемся со смещением и получаем примерно такую картину:

![paint](./static/paint.png)


**3. Надпись в notepad++**
Для получения третьей части дампим память процесса notepad++.exe при помощи memdump и при помощи strings ищем строчку с третьей частью (можно воспользоваться grep'ом по строке "haha get another way")
```
python2 vol.py -f memzilla3.mem --profile=Win7SP1x64 memdump --pid=3852 --dump-dir=./dumps
```

![notepad](./static/notepad.png)










## Flag
mctf{OhMYG0D_F0r3n5iccc_i5_s0_CO0OOO00OO0OL!!!!!}

## Complexity
Hard
