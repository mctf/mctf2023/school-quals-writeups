# mctf-adam-ruinz-everything
Forensic task for MCTF Junior 
# Опсиание 
Мой друг Адам опять нахваливал то старьё, не могу уже вспомнить название. В этот раз он скинул мне какой-то файл и сказал как что круто ею овладел.

**Выдать файл с трафиком**
# Решение 
Открываем трафик и видим что все данные – USB трафик.
![prto](./static/proto.png)
Чтобы раскодировать трафик нам понадобится python и tshark.
Скачиваем [скрипт](https://github.com/carlospolop-forks/ctf-usb-keyboard-parser/blob/master/usbkeyboard.py) расшифровки с Github. Далее выполняем фильтрацию строк в трафике с помощью tshark 
```
tshark -r ./from_adam.pcap -Y 'usb.capdata && usb.data_len == 8' -T fields -e usb.capdata | sed 's/../:&/g2' > keystrokes.txt
```
потом кидаем скрипт на отфильрованные строки 
```
python3 usbkeyboard.py ./keystrokes.txt
```
и получаем 
```
snrweq }][anotmctf{V1jeM_ru1Nzsc6552_4v3ry7H1Ngdhrn␛
^
vlllllld
vy$hhhhp
lv$d
12hv5hd
:%s/je//g
v5ld
x
:%s/4/3/gc
y       
```
Первая строка – испорченный флаг, остальные – комнады vim. Необходимо ввести строку в редактор, выполнить поочердно команды и получить флаг.

Источник инструкции: [Как расшифровать трафик USB](https://book.hacktricks.xyz/generic-methodologies-and-resources/basic-forensic-methodology/pcap-inspection/usb-keystrokes)

# Flag 
```
mctf{V1M_ru1Nz_3v3ry7H1Ng}
```
