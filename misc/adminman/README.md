# Adminman

## Writetup
Мы получаем какой-то файл, давайте проверим его тип:
file mctf_task
```
mctf_task: Linux rev 1.0 ext4 filesystem data, UUID=3dda8d8a-2b18-44b3-bd4f-159d19cee288 (extents) (64bit) (large files) (huge files)
```
Ага... кажется это нужно примаунтить... давайте попробуем
mount mctf_task /mnt

Проверяем, что все ок 
df -h
```
/dev/loop0         1,8M         708K  906K           44% /mnt
```
Как видим, у нас появилась новая директория, теперь попытаемся зайти внутрь...
```
cd /mnt
```
Отлично, теперь нам осталось просто исследовать, все что есть внутри, находим .bash_history 
Анализируем историю и видим там вот такую интересную команду: 
```
curl https://pastebin.com/WpEuhiWW
```
Переходим по ссылке и видим флаг :)

## Flag
```
mctf{eaaaaaaaaaaaasyyyyyyyyyyy_adm1n} 
```