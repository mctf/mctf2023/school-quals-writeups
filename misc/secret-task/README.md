# secret task

## Description
Очень секретный флаг, нужно постараться его найти… но я верю в вас… вы сможете, а если не сможете, то перекусите капустой… и возвращайтесь!

## Writeup
Обращаем внимание на что в авторе написано:
```
Author:
hashmepls 
```
Давайте попробуем перевести фразу hashmepls в md5 и сдать:
```
e6ee1b4f7ae29cddbc1e221e3ee3c4bf
```

Done

## Flag
mtcf{e6ee1b4f7ae29cddbc1e221e3ee3c4bf}