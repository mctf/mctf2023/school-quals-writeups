# OhMyPosh

## Description

\<undefined>

## Writeup

Все методы обфускации были так или иначе описаны [здесь](https://github.com/t3l3machus/PowerShell-Obfuscation-Bible).

## Flag

**mctf{P0W3r_Of_p0VVeR$he11}**
