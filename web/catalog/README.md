# CATalog

о да это же мое любимое приложения для просмотра котов

## Description

в прошлом команда по созданию елочных игрушек образовала концерн CAT (Cat analysis troops) и вам дают опробывать их новое приложение по просмотру котов

## Выкатка

Ничего сложного нет
флаг лежит в docker-compose.yaml
поднимается docker-compose up
усе

## Writeup

На главной странице лежит подсказка что используется EJS

Также есть ручка /flag -> дергаем -> доступа нет :c

Заходим в upload

Ручка upload уязвима к пейлоаду ```<%= 2 + 2 %>```

Заходим на [ejs](https://ejs.co/) находим это чудо ```<%- include('path'); %>```

Пытаемся достать ручку флага таким образом

```<%- include('../flag'); %>``` -> достает флаг

гг молодцы!