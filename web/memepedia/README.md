# MemePedia | Hard | Web
## Description 
Мы решили что вам нужно отдохнуть и расслабиться поэтому сделали сайт с мемами. Проведите время с удовольствием.

## Writeup 
В процессе чтения истории и описания мемов замечаем что img получается с помощью get параметра.
Здесь заложен LFI позволяющий считывать любые файлы в системе.
```
<img src="?image=images/mem1.jpg" alt="Spinning cat" />
```
Так как флаг находиться в файле с неизвестным нам путем, нужно получить rce

RFI не приносит плодов после поисков находим LFI2RCE
https://book.hacktricks.xyz/pentesting-web/file-inclusion/lfi2rce-via-php-filters

В репозитории приложен POC.py позволяющий добиться RCE

Получаем флаг:)
## Flag 
```
mctf{1_L0v3_M3M3s_s0_Much}
```