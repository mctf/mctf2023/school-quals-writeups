# Heisenberg

## Descripton

> ~~История~~ Химия – самый лучший учитель, у которого самые плохие ученики».
> (c) Индира Ганди

## Writeup

Видим массив data

![data](image-1.png)

Видим логику

![logic](image.png)

Особенность заключается в том что любой двумерный массив `arr[i][j]` можно представить как `arr[i * size + j]`

в случае с argv, `argv[0]` это название исполняемого файла а `argv[1]` это указатель на аргумент с которым исполняется файл

по итогу следующие массивы равносильны

```c
{{"./heisenberg"},      // argv[0]   
 {"MCTF{AAAAAAAAAAA}"}} // argv[1]

// argv[0]    argv[0 * size + 12]
{"./heisenbergMCTF{AAAAAAAAAAA}"}
```

### Логика:

1) XOR названия с массивом data

2) сравнение с помощью XOR
если (a ^ b) = 0 и a == b то 0 ^ 1 = 1

3) если MUST_BE_ONE == 1 то, бинарник подтверждает что флаг правильный.

### Решение:

Решение в представлено в [solve.py](solve.py)

```python
a = [124, 9, 60, 54, 62, 60, 49, 44, 37, 63, 34]
b = [ord(i) for i in "/heisenberg"]

res = []
for i in range(len(a)):
    res += chr(a[i] ^ b[i])
print('MCTF{' + ''.join(res) + '}')
```

### Flag

MCTF{SaY_MY_N@ME}