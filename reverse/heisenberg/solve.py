a = [124, 9, 60, 54, 62, 60, 49, 44, 37, 63, 34]
b = [ord(i) for i in "/heisenberg"]

res = []
for i in range(len(a)):
    res += chr(a[i] ^ b[i])
print('MCTF{' + ''.join(res) + '}')
