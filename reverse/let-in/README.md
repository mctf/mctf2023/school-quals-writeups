# Let In

# Flag
MCTF{L37_M3_0U7_now}

# Complexity
Medium

# Administration
Требуется выдать файл LetIn.apk

# WriteUp
При открытии приложения нам предлагается ввести пин-код:<br>
![Alt text](images/image.png)

Так как пин-кода у нас нет, переходим в jadx-gui и открываем MainActivity.<br>
В данном классе мы можем найти метод CheckPasscode, которая принимает в себя строку и сравнивает ее с "1111".
![Alt text](images/image-1.png)

Следовательно понимаем, что пин-код к приложению - 1111, входим и видим следующее:
![Alt text](images/image-2.png)

Декодим через base64 данную строку, получаем:
![Alt text](images/image-3.png)

И через Rot13:
![Alt text](images/image-4.png)