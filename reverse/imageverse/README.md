# ImageVerse

## Description

Разработчики с каждым годом все улучшают и улучшают методы защиты своего ПО, но в этот раз, похоже, зашли в тупик…

## Writeup

First of all, let's analyze program using DiE. It was written on .NET, so we can use dnSpy to look inside it.

The program is rather small, but we can notice some interesting things:
1. Program gets some data from remote resource.
2. Some byte operations going on.
3. Result of byte operations loads using *Assembly.Load*.

if we try to look what's lying on remote resource, we'll see just a picture.But if we'll try to look on it's bytes in hex editor, we can notice that picture has embedded executable bytes.

Inside this executable (it also written on .NET), we can see a large amount of int's and some checks using them as chars.

Decoding them and getting credentials - eMax5j37:R0tk1Ng

After that, all that remains is to run the original application and get the flag.

## Flag

**mctf{r3Ver53_In_1m4Ges_i5_fUN}**
