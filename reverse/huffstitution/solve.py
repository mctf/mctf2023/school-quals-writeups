codes = [
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "0000000",
    "1000010",
    "1000011",
    "1000100",
    "1000101",
    "1000110",
    "1000111",
    "1001000",
    "1001001",
    "1001010",
    "1001011",
    "1001100",
    "1001101",
    "1001110",
    "1001111",
    "1010000",
    "1010001",
    "1010010",
    "1010011",
    "1010100",
    "1010101",
    "1010110",
    "1010111",
    "1011000",
    "1011001",
    "1011010",
    "1011011",
    "1011100",
    "1011101",
    "1011110",
    "1011111",
    "1100000",
    "1100001",
    "1100010",
    "1100011",
    "1100100",
    "1100101",
    "1100110",
    "1100111",
    "1101000",
    "1101001",
    "1101010",
    "1101011",
    "1101100",
    "1101101",
    "1101110",
    "1101111",
    "1110000",
    "1110001",
    "1110010",
    "1110011",
    "1110100",
    "1110101",
    "1110110",
    "1110111",
    "1111000",
    "1111001",
    "1111010",
    "1111011",
    "1111100",
    "1111101",
    "1111110",
    "1111111",
    "000000",
    "000001",
    "000010",
    "000011",
    "000100",
    "000101",
    "000110",
    "000111",
    "001000",
    "001001",
    "001010",
    "001011",
    "001100",
    "001101",
    "001110",
    "001111",
    "010000",
    "010001",
    "010010",
    "010011",
    "010100",
    "010101",
    "010110",
    "010111",
    "011000",
    "011001",
    "011010",
    "011011",
    "011100",
    "011101",
    "011110",
    "011111",
    "100000",
    "0000000"
]
flag = ("0011110001010101100010000111011010011010101000001101100100101"
        "0110101101010100000100110011101111010111101100100000101010110"
        "1001100111101001000111010101010000011010111010111110010001010"
        "110110011010011111011001011101011010100111010010010000011111")

curr_segment = ""
decoded_flag = ""
i = 0

while i < len(flag):
    curr_segment += flag[i]
    if curr_segment in codes:
        for j in range(len(codes)):
            if curr_segment == codes[j]:
                decoded_flag += chr(j)
                curr_segment = ""
    i += 1

print(decoded_flag)
