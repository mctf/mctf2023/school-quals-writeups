#Name: Ооооо, опять эта крипта...
#Описание: 
#Пишу я админу:"Че вы сделали? Вот, посмотри, оно не работает." 
#А он мне отвечает:"Все работает! Переверни и смотри!"

#encoder

import base64
'''
flag='MCTF{Oyy_Y0u_kNoW_AboUt_SH1fT'+'}'

f=[format(ord(i),'b') for i in flag]
dec=[int(f[i],2)<<2 for i in range(len(f))]

cipher=''.join([chr(dec[i]) for i in range(len(dec))])
fin_cipher=base64.b64encode(cipher.encode('utf-8'))

fin_cipher=__import__('base64').b64encode(bytes(''.join([chr(ord(i) << 2) for i in flag]).encode()))
print(fin_cipher)

#decoder'''
first_dec=base64.b64decode('xLTEjMWQxJjHrMS8x6THpMW8xaTDgMeUxbzGrMS4xrzFnMW8xITGiMa8xZTHkMW8xYzEoMOExpjFkMe0').decode('utf-8')
c=[format(ord(i),'b') for i in first_dec]
ans=[int(c[i],2)>>2 for i in range(len(c))]
flag=''.join([chr(ans[i]) for i in range(len(ans))])
print(flag)

# print(__import__('base64').b64encode(bytes(''.join([chr(ord(i) << 2) for i in 'MCTF{Oyy_Y0u_kNoW_AboUt_SH1fT}']).encode())))
