## Task Name

Basecrypto
## Description

Пришел я на ивент, зарегался, начал играть, смотрю таск и пишу админу:“ Что это? Что вы сделали? Оно вообще работает?”  
А он мне отвечает:“ Все работает! Посмотри и выполни!”
Поможешь мне понять, как получить флаг?

```
print(__import__('base64').b64encode(bytes(''.join([chr(ord(i) << 2) for i in 'MCTF{...}']).encode())))
>>>xLTEjMWQxJjHrMS8x6THpMW8xaTDgMeUxbzGrMS4xrzFnMW8xITGiMa8xZTHkMW8xYzEoMOExpjFkMe0
```
## [](#writeups)Writeups

Во-первых, разберем что именно делает скрипт, благо он написан на питоне:
1. Берет каждый символ флага и проводит побитовый сдвиг влево на два бита, после чего все измененные символы добавляет в одну строку
2. Полученную строку кодирует в base64
После того, как мы разобрали, что делает скрипт, надо придумать, как получить флаг обратно

Тк base64 - кодировка, то раскодировать строку не составит у нас труда

После этого вспоминаем/узнаем, что побитовый сдвиг влево добавляет нули к двоичному виду символа, то есть, если просто убрать их(например, сдвигом вправо), то мы получим исходные символы, а значит и флаг!

Вот базовый дешифратор:
```
from base64 import b64decode

encoded_flag = b64decode(b'xLTEjMWQxJjHrMS8x6THpMW8xaTDgMeUxbzGrMS4xrzFnMW8xITGiMa8xZTHkMW8xYzEoMOExpjFkMe0').decode('utf-8')

for char in encoded_flag:
    print(chr(ord(char) >> 2), end='')
```

## [](#flag)Flag

MCTF{Oyy_Y0u_kNoW_AboUt_SH1fT}

## [](#complexity)Complexity

Easy