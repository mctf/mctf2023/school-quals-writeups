from algorithm import decrypt


def split_data(data, n):
    return [data[i:i + n] for i in range(0, len(data), n)]


with open('hash.txt') as file:
    data = file.read()

data = split_data(data, 2)

for chunk in data:
    if chunk[0] == 'm':
        flag = chunk[0]
        pos = decrypt(chunk)

        while pos < len(data):
            if not 'mctf'.startswith(flag[:4]):
                break

            if data[pos][0] == 'w':
                print(flag.replace('v', '{') + '}')
                break

            flag += data[pos][0]
            pos = decrypt(data[pos])
